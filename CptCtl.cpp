// CptCtl.cpp : Implementation of the CCptCtrl ActiveX Control class.

#include "stdafx.h"
#include "cpt.h"
#include "CptCtl.h"
#include "CptPpg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CCptCtrl, COleControl)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CCptCtrl, COleControl)
	//{{AFX_MSG_MAP(CCptCtrl)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
	ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Dispatch map

BEGIN_DISPATCH_MAP(CCptCtrl, COleControl)
	//{{AFX_DISPATCH_MAP(CCptCtrl)
	DISP_FUNCTION(CCptCtrl, "Event", Event, VT_BSTR, VTS_BSTR VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CCptCtrl, "Update", Update, VT_BSTR, VTS_BSTR)
	//}}AFX_DISPATCH_MAP
	DISP_FUNCTION_ID(CCptCtrl, "AboutBox", DISPID_ABOUTBOX, AboutBox, VT_EMPTY, VTS_NONE)
END_DISPATCH_MAP()


/////////////////////////////////////////////////////////////////////////////
// Event map

BEGIN_EVENT_MAP(CCptCtrl, COleControl)
	//{{AFX_EVENT_MAP(CCptCtrl)
	// NOTE - ClassWizard will add and remove event map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_EVENT_MAP
END_EVENT_MAP()


/////////////////////////////////////////////////////////////////////////////
// Property pages

// TODO: Add more property pages as needed.  Remember to increase the count!
BEGIN_PROPPAGEIDS(CCptCtrl, 1)
	PROPPAGEID(CCptPropPage::guid)
END_PROPPAGEIDS(CCptCtrl)


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CCptCtrl, "CPT.CptCtrl.1",
	0xb4797b1a, 0xd1cb, 0x46f9, 0x90, 0xb5, 0x8b, 0x42, 0xff, 0xad, 0x65, 0xb3)


/////////////////////////////////////////////////////////////////////////////
// Type library ID and version

IMPLEMENT_OLETYPELIB(CCptCtrl, _tlid, _wVerMajor, _wVerMinor)


/////////////////////////////////////////////////////////////////////////////
// Interface IDs

const IID BASED_CODE IID_DCpt =
		{ 0xec10c3fd, 0xe70b, 0x41c2, { 0x8b, 0x3, 0xf3, 0x1, 0xe5, 0x2, 0x94, 0x10 } };
const IID BASED_CODE IID_DCptEvents =
		{ 0xd98dfe1b, 0x74e8, 0x44b7, { 0x94, 0x3a, 0x63, 0xba, 0xd1, 0x42, 0x9d, 0xf4 } };


/////////////////////////////////////////////////////////////////////////////
// Control type information

static const DWORD BASED_CODE _dwCptOleMisc =
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CCptCtrl, IDS_CPT, _dwCptOleMisc)


/////////////////////////////////////////////////////////////////////////////
// CCptCtrl::CCptCtrlFactory::UpdateRegistry -
// Adds or removes system registry entries for CCptCtrl

BOOL CCptCtrl::CCptCtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO: Verify that your control follows apartment-model threading rules.
	// Refer to MFC TechNote 64 for more information.
	// If your control does not conform to the apartment-model rules, then
	// you must modify the code below, changing the 6th parameter from
	// afxRegApartmentThreading to 0.

	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_CPT,
			IDB_CPT,
			afxRegApartmentThreading,
			_dwCptOleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}


/////////////////////////////////////////////////////////////////////////////
// CCptCtrl::CCptCtrl - Constructor

CCptCtrl::CCptCtrl()
{
	InitializeIIDs(&IID_DCpt, &IID_DCptEvents);
	m_target = 0;
	// TODO: Initialize your control's instance data here.
}


/////////////////////////////////////////////////////////////////////////////
// CCptCtrl::~CCptCtrl - Destructor

CCptCtrl::~CCptCtrl()
{
	// TODO: Cleanup your control's instance data here.
}


/////////////////////////////////////////////////////////////////////////////
// CCptCtrl::OnDraw - Drawing function

void CCptCtrl::OnDraw(
			CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid)
{
	// TODO: Replace the following code with your own drawing code.
	pdc->FillRect(rcBounds, CBrush::FromHandle((HBRUSH)GetStockObject(WHITE_BRUSH)));
	pdc->Ellipse(rcBounds);
}


/////////////////////////////////////////////////////////////////////////////
// CCptCtrl::DoPropExchange - Persistence support

void CCptCtrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

	// TODO: Call PX_ functions for each persistent custom property.

}


/////////////////////////////////////////////////////////////////////////////
// CCptCtrl::OnResetState - Reset control to default state

void CCptCtrl::OnResetState()
{
	COleControl::OnResetState();  // Resets defaults found in DoPropExchange

	// TODO: Reset any other control state here.
}


/////////////////////////////////////////////////////////////////////////////
// CCptCtrl::AboutBox - Display an "About" box to the user

void CCptCtrl::AboutBox()
{
	CDialog dlgAbout(IDD_ABOUTBOX_CPT);
	dlgAbout.DoModal();
}



static bool ParseUInt(INT_PTR& p, LPCTSTR s)
{
    if (!s)
        return false;
    p = 0;
    do {
        char c = *s;
        if (c < '0' || c > '9')
            return false;
        p = p * 10 + (c - '0');
    } while (*++s);
    return true;
}


/////////////////////////////////////////////////////////////////////////////
// CCptCtrl message handlers

BSTR CCptCtrl::Event(LPCTSTR sender, LPCTSTR type, LPCTSTR data)
{
	CString strResult;

	if (!IsWindow(m_target) || type ==	NULL || *type == 0)
		return strResult.AllocSysString();

	if (lstrcmpi(type, "event_handler") == 0) {
		INT_PTR target = 0;
		if (!ParseUInt(target, data)) {
			strResult = "throw new Error('event handler update fail!');";
			return strResult.AllocSysString();
		}
		m_target = (HWND) target;
		strResult = "restart();";
		return strResult.AllocSysString();
	}

	CString fmt;
	fmt.Format("%s::%s", (sender && *sender)? sender:"global", type);

	int cc = lstrlen(fmt) + 1;

	HGLOBAL hgl_e = ::GlobalAlloc(GMEM_MOVEABLE, cc * sizeof(TCHAR));
	if (!hgl_e)
		return strResult.AllocSysString();

	LPTSTR pstr = (LPTSTR) ::GlobalLock(hgl_e);
	if (!pstr) {
		::GlobalFree(hgl_e);
		return strResult.AllocSysString();
	}
	
	::lstrcpy(pstr, fmt);
	::GlobalUnlock(hgl_e);

	
	if (!data || *data == 0) {
		HGLOBAL res = (HGLOBAL) ::SendMessage(m_target, WM_USER+124, (WPARAM) hgl_e, 0);
		::GlobalFree(hgl_e);
		if (!res)
			return strResult.AllocSysString();

		pstr = (LPTSTR) ::GlobalLock(res);
		if (!pstr) {
			::GlobalFree(res);
			return strResult.AllocSysString();
		}

		strResult = pstr;
		::GlobalUnlock(res);
		::GlobalFree(res);

		::GlobalFree(hgl_e);
		return strResult.AllocSysString();
	}

	cc = lstrlen(data) + 1;
	HGLOBAL hgl_d = ::GlobalAlloc(GMEM_MOVEABLE, cc * sizeof(TCHAR));
	if (!hgl_d)
		return strResult.AllocSysString();


	pstr = (LPTSTR) ::GlobalLock(hgl_d);
	if (!pstr) {
		::GlobalFree(hgl_d);
		return strResult.AllocSysString();
	}

	::lstrcpy(pstr, data);
	::GlobalUnlock(hgl_d);

	HGLOBAL res = (HGLOBAL) ::SendMessage(m_target, WM_USER+124, (WPARAM) hgl_e, (LPARAM) hgl_d);
	
	::GlobalFree(hgl_d);
	::GlobalFree(hgl_e);

	if (!res)
		return strResult.AllocSysString();

	pstr = (LPTSTR) ::GlobalLock(res);
	if (!pstr) {
		::GlobalFree(res);
		return strResult.AllocSysString();
	}

	strResult = pstr;
	::GlobalUnlock(res);
	::GlobalFree(res);

	return strResult.AllocSysString();
}


BSTR CCptCtrl::Update(LPCTSTR target) 
{
	CString strResult;
	
	strResult.Format("<b>Connection to [%d] lost...</b>", (int) m_target);

	int cc = lstrlen(target);

	if (!IsWindow(m_target) || cc <= 0)
		return strResult.AllocSysString();

	++cc;
	HGLOBAL hgl = ::GlobalAlloc(GMEM_MOVEABLE, cc * sizeof(TCHAR));
	if (!hgl)
		return strResult.AllocSysString();

	LPTSTR pstr = (LPTSTR) ::GlobalLock(hgl);
	if (!pstr) {
		::GlobalFree(hgl);
		return strResult.AllocSysString();
	}

	::lstrcpy(pstr, target);
	::GlobalUnlock(hgl);

	HGLOBAL res = (HGLOBAL) ::SendMessage(m_target, WM_USER+123, 0, (LPARAM) hgl);

	::GlobalFree(hgl);
	
	if (!res)
		return strResult.AllocSysString();

	pstr = (LPTSTR) ::GlobalLock(res);
	if (!pstr) {
		::GlobalFree(res);
		return strResult.AllocSysString();
	}
	strResult = pstr;
	::GlobalFree(res);

	return strResult.AllocSysString();
}
