#if !defined(AFX_CPT_H__571FD9D3_BB7A_4484_8AF9_D87713DCB99F__INCLUDED_)
#define AFX_CPT_H__571FD9D3_BB7A_4484_8AF9_D87713DCB99F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// cpt.h : main header file for CPT.DLL

#if !defined( __AFXCTL_H__ )
	#error include 'afxctl.h' before including this file
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CCptApp : See cpt.cpp for implementation.

class CCptApp : public COleControlModule
{
public:
	BOOL InitInstance();
	int ExitInstance();
};

extern const GUID CDECL _tlid;
extern const WORD _wVerMajor;
extern const WORD _wVerMinor;

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CPT_H__571FD9D3_BB7A_4484_8AF9_D87713DCB99F__INCLUDED)
