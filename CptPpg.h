#if !defined(AFX_CPTPPG_H__4FEB56D1_6D60_4C14_96B4_139295D298EF__INCLUDED_)
#define AFX_CPTPPG_H__4FEB56D1_6D60_4C14_96B4_139295D298EF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// CptPpg.h : Declaration of the CCptPropPage property page class.

////////////////////////////////////////////////////////////////////////////
// CCptPropPage : See CptPpg.cpp.cpp for implementation.

class CCptPropPage : public COlePropertyPage
{
	DECLARE_DYNCREATE(CCptPropPage)
	DECLARE_OLECREATE_EX(CCptPropPage)

// Constructor
public:
	CCptPropPage();

// Dialog Data
	//{{AFX_DATA(CCptPropPage)
	enum { IDD = IDD_PROPPAGE_CPT };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA

// Implementation
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Message maps
protected:
	//{{AFX_MSG(CCptPropPage)
		// NOTE - ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CPTPPG_H__4FEB56D1_6D60_4C14_96B4_139295D298EF__INCLUDED)
