#if !defined(AFX_CPTCTL_H__5F704281_C835_4F5C_8FB1_63679764E7B0__INCLUDED_)
#define AFX_CPTCTL_H__5F704281_C835_4F5C_8FB1_63679764E7B0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// CptCtl.h : Declaration of the CCptCtrl ActiveX Control class.

/////////////////////////////////////////////////////////////////////////////
// CCptCtrl : See CptCtl.cpp for implementation.

class CCptCtrl : public COleControl
{
	DECLARE_DYNCREATE(CCptCtrl)

// Constructor
public:
	CCptCtrl();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCptCtrl)
	public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();
	//}}AFX_VIRTUAL

// Implementation
protected:
	~CCptCtrl();

	DECLARE_OLECREATE_EX(CCptCtrl)    // Class factory and guid
	DECLARE_OLETYPELIB(CCptCtrl)      // GetTypeInfo
	DECLARE_PROPPAGEIDS(CCptCtrl)     // Property page IDs
	DECLARE_OLECTLTYPE(CCptCtrl)		// Type name and misc status

// Message maps
	//{{AFX_MSG(CCptCtrl)
		// NOTE - ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Dispatch maps
	//{{AFX_DISPATCH(CCptCtrl)
	afx_msg BSTR Event(LPCTSTR sender, LPCTSTR type, LPCTSTR data);
	afx_msg BSTR Update(LPCTSTR target);
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()

	afx_msg void AboutBox();

// Event maps
	//{{AFX_EVENT(CCptCtrl)
	//}}AFX_EVENT
	DECLARE_EVENT_MAP()

// Dispatch and event IDs
public:
	enum {
	//{{AFX_DISP_ID(CCptCtrl)
	dispidEvent = 1L,
	dispidUpdate = 2L,
	//}}AFX_DISP_ID
	};
private:
	HWND m_target;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CPTCTL_H__5F704281_C835_4F5C_8FB1_63679764E7B0__INCLUDED)
