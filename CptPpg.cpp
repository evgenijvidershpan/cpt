// CptPpg.cpp : Implementation of the CCptPropPage property page class.

#include "stdafx.h"
#include "cpt.h"
#include "CptPpg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CCptPropPage, COlePropertyPage)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CCptPropPage, COlePropertyPage)
	//{{AFX_MSG_MAP(CCptPropPage)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CCptPropPage, "CPT.CptPropPage.1",
	0x24aac2d7, 0x3bf7, 0x4831, 0x98, 0xb4, 0x30, 0x39, 0x4d, 0x30, 0x1f, 0xd5)


/////////////////////////////////////////////////////////////////////////////
// CCptPropPage::CCptPropPageFactory::UpdateRegistry -
// Adds or removes system registry entries for CCptPropPage

BOOL CCptPropPage::CCptPropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_CPT_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}


/////////////////////////////////////////////////////////////////////////////
// CCptPropPage::CCptPropPage - Constructor

CCptPropPage::CCptPropPage() :
	COlePropertyPage(IDD, IDS_CPT_PPG_CAPTION)
{
	//{{AFX_DATA_INIT(CCptPropPage)
	// NOTE: ClassWizard will add member initialization here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_INIT
}


/////////////////////////////////////////////////////////////////////////////
// CCptPropPage::DoDataExchange - Moves data between page and properties

void CCptPropPage::DoDataExchange(CDataExchange* pDX)
{
	//{{AFX_DATA_MAP(CCptPropPage)
	// NOTE: ClassWizard will add DDP, DDX, and DDV calls here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_MAP
	DDP_PostProcessing(pDX);
}


/////////////////////////////////////////////////////////////////////////////
// CCptPropPage message handlers
